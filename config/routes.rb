# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

Rails.application.routes.draw do
	require 'sidekiq/web'
	Sidekiq::Web.use Rack::Auth::Basic do |username, password|
		ActiveSupport::SecurityUtils.secure_compare(::Digest::SHA256.hexdigest(username), ::Digest::SHA256.hexdigest(Rails.application.secrets.sedekiq_username)) &
		ActiveSupport::SecurityUtils.secure_compare(::Digest::SHA256.hexdigest(password), ::Digest::SHA256.hexdigest(Rails.application.secrets.sedekiq_password))
	end
	mount Sidekiq::Web, at: '/sidekiq'

	use_doorkeeper
	devise_for :users, ActiveAdmin::Devise.config.merge(
		skip: [:confirmations, :registrations, :unlocks]
	)
	ActiveAdmin.routes(self)

	get 'terms_and_conditions' => 'terms_and_conditions#show'
	get 'privacy_policies' => 'privacy_policies#show'

	scope module: 'api' do
		scope '1', module: 'v1' do
			resources :users do
				patch 'update_device_token' => 'devices#update_device_token'
      end

      resources :projects, only: [:create, :index] do
				resources :documents, only: [:create]
      end

			devise_scope :user do
				post 'users/forgot_password' => 'users#forgot_password'
				post 'users/sign_up' => 'registrations#create'
				get 'reset_password' => 'passwords#new'
				get 'change_password' => 'passwords#edit'
				put 'update_user_password' => 'passwords#update'
			end
		end
	end
end
