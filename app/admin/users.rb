ActiveAdmin.register User do
	menu priority: 2, label: I18n.t('active_admin.page_views.users'),
		if: proc { current_user.has_role? :admin }

	permit_params :email, :name, :avatar

	filter :email
	filter :name

	config.per_page = 25

	before_create :set_user_role
	controller do
		def scoped_collection
			User.with_role :user
		end

		def set_admin_role( user )
			user.add_role :user
		end
	end

	action_item :view, only: :show do
		link_to I18n.t('active_admin.back'), collection_path
	end
	index download_links: [:csv] do
		selectable_column
		id_column
		column :email
		column :name
		actions
	end

	form do |f|
		f.inputs I18n.t('active_admin.form_title.user') do
			f.semantic_errors *f.object.errors.keys
			f.input :email
			f.input :name
		end
		f.actions
	end

	show do |user|
		attributes_table do
			row :avatar do
				image_tag(
					user.avatar, class: 'user-thumb', size: "100"
				) if user.avatar.present?
			end
			row :email
			row :name
		end
	end

	csv do
		column :id
		column :email
		column :name
	end
end
