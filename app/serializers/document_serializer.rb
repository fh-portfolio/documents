class DocumentSerializer < ActiveModel::Serializer
  attributes :id, :file_url, :description
end
