class LogOutUser < Interactor
	validates :token, :tokens_controller, presence: true

	def self.with( token:, tokens_controller: )
		log_out_user = new token: token, tokens_controller: tokens_controller
		log_out_user.execute
	end

	def execute
		validate_access_token
		log_out
	end

	private

	def user
		User.find access_token.resource_owner_id
	end

	def log_out
		user&.device&.destroy!
		tokens_controller.revoke_user_token
	end

	def access_token
		@access_token ||= Doorkeeper::AccessToken.find_by token: token
	end

	def access_token_revoked?
		access_token.revoked_at.nil?
	end

	def validate_access_token
		invalid :access_token, 'The access token doesn\'t exist' if access_token.nil?
		forbidden :token, 'The token is already revoked' unless access_token_revoked?
	end
end
