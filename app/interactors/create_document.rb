class CreateDocument < Interactor
  class << self
    def with project:, file:, description: nil
      interactor = self.new project: project, file: file, description: description
      interactor.execute
    end
  end

  def execute
    Document.create! project: project, file: file, description: description
  end
end
