class GetProjects < Interactor
  def self.ownedBy owner:
    interactor = self.new owner: owner
    interactor.execute
  end

  def execute
    Project.where owner: owner
  end
end