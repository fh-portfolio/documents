class SignUpUser < Interactor
	validates :email, :password, :name, presence: true

	def self.with( user: , email: , password: , name: )
		sign_up_user = new(
			user: user, email: email, password: password, name: name
		)
		sign_up_user.execute
	end

	def execute
		user.save
		if user.persisted?
			user.add_role :user
			user
		else
			invalid :sign_up, user.errors.full_messages
		end
	end
end
