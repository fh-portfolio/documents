class CreateProject < Interactor
  def self.with name:, description:, owner:
    interactor = self.new name: name, description: description, owner: owner
    interactor.execute
  end

  def execute
    Project.create! name: name, description: description, owner: owner
  end
end