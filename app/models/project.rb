# == Schema Information
#
# Table name: projects
#
#  id          :integer          not null, primary key
#  name        :string           not null
#  description :text             not null
#  owner_id    :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_projects_on_name      (name)
#  index_projects_on_owner_id  (owner_id)
#
# Foreign Keys
#
#  fk_rails_...  (owner_id => users.id) ON DELETE => cascade
#

class Project < ApplicationRecord
  belongs_to :owner, class_name: 'User'

  validates :name, uniqueness: true, presence: true
  validates :description, presence: true
  validates :owner, presence: true

  has_many :documents
end
