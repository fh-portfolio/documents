class Document < ApplicationRecord
  has_attached_file :file

  belongs_to :project

  validates_presence_of :file, :project
  validate :validate_file_name_uniqueness

  #.tiff .jpg	.png .raw	.dng .crw	.nef .mef	.iiq .eip
  #.indd .ai .doc	.csv .xls
  validates_attachment_file_name :file, matches: [/docx?\z/, /xlsx?\z/, /pdf\z/, /txt\z/]

  scope :by_filename, -> (basename) { where "file_file_name LIKE ?", ("#{basename}.%")}

  def file_url
    Settings.urls.base + file.url
  end

  def validate_file_name_uniqueness
    errors.add(:file, 'filenames must be unique') unless file.blank? ||
        (file.present? && Document.by_filename(basename).empty?)
  end

  private
  def basename
    File.basename(file_file_name, File.extname(file_file_name))
  end
end
