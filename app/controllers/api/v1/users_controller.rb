class Api::V1::UsersController < Api::V1::ApiController
	before_action :doorkeeper_authorize!, except: [ :forgot_password ]

	def show
		render_successful_response current_user, UserSerializer
	end

	def forgot_password
		user_by_email.send_reset_password_instructions
		render_successful_empty_response
	end

	private

	def user_by_email
		user = User.find_by( email: params[:email] )
		not_found :user, "The user doesn\'t exist" unless user.present?
		user
	end
end
