class Api::V1::ProjectsController < Api::V1::ApiController
  before_action :doorkeeper_authorize!

  def index
    projects = GetProjects.ownedBy owner: current_user
    render_successful_response projects, ProjectSerializer
  end

  def create
    response = CreateProject.with create_params
    render_successful_response response
  end

  def create_params
    {
      owner: current_user,
      name: params[:name],
      description: params[:description],
    }
  end

  def projects
    Project.all
  end
end
