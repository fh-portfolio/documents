class Api::V1::DocumentsController < Api::V1::ApiController
  before_action :doorkeeper_authorize!

  def create
    response = CreateDocument.with project: project_from_params, file: params[:file], description: params[:description]
    render_successful_response response, DocumentSerializer
  end

  private
  def project_from_params
    @project ||= Project.find(params[:project_id])
  end
end
