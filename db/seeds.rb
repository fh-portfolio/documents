unless User.find_by( email: Rails.application.secrets.admin_user ).present?
	puts 'creating Admin...'
	admin_user = User.create!(
			email: Rails.application.secrets.admin_user,
			password: Rails.application.secrets.admin_pass,
			password_confirmation: Rails.application.secrets.admin_pass
		)
	admin_user.add_role :admin
	puts 'Done'
end
