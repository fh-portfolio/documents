class CreateDocuments < ActiveRecord::Migration[5.1]
  def change
    create_table :documents do |t|
      t.text :description
      t.attachment :file, null: false
      t.references :project, index: true, null: true, foreign_key: true

      t.timestamps
    end
  end
end
