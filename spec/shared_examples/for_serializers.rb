RSpec.shared_examples 'expect correct serialization' do
	it 'serialize the object correctly' do
		serialization = ActiveModelSerializers::SerializableResource.new( serializable_object ).as_json
		expect( serialization ).to eq expected_serialization
	end
end