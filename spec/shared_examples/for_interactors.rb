# Create Object Interactor
RSpec.shared_examples 'interactor creates the object' do
		it 'creates the object' do
			expect{ create_object }.to change{model.count}.by 1
		end
end

RSpec.shared_examples 'interactor creates the objects' do
		it 'creates the object' do
			expect{ create_object }.to change{model.count}.by amount
		end
end

RSpec.shared_examples 'interactor does not create a new object' do
		it 'does not creates the object' do
			expect{ create_object }.to change{model.count}.by 0
		end
end

RSpec.shared_examples 'interactor creates the object with the proper attributes' do
	let(:interactor_execution) { create_object }
	include_examples 'interactor set the object with the proper attributes'
end

RSpec.shared_examples 'interactor updates the object with the proper attributes' do
	let(:interactor_execution) { update_object }
	include_examples 'interactor set the object with the proper attributes'
end

RSpec.shared_examples 'interactor set the object with the proper attributes' do
	it 'set the object with the proper attributes' do
		expect( interactor_execution ).to have_attributes(expected_attributes)
	end
end

RSpec.shared_examples 'create object raise record invalid exception' do
	it 'raise an exception' do
		expect{ create_object }.to raise_error ActiveRecord::RecordInvalid
	end
end


RSpec.shared_examples 'delete interactor deletes the object' do
	it 'the object is deleted' do
		delete_interactor.with params
		expect( object_class.all ).to_not include object
	end
end

RSpec.shared_examples 'delete interactor raise error' do |error|
	it {
		expect {
			delete_interactor.with params
		}.to raise_exception error
	}
end

RSpec.shared_examples 'delete interactor examples' do
	let(:params) { { object_class.name.downcase.to_sym => object } }

	context 'with correct params' do
		include_examples 'delete interactor deletes the object'
	end
end

