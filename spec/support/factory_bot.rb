RSpec.configure do |config|
	config.include FactoryBot::Syntax::Methods

	config.before(:suite) do
		DatabaseCleaner.strategy = :transaction
		DatabaseCleaner.clean_with(:truncation)
		# FactoryBot.lint
	end

	config.around(:each) do |example|
		DatabaseCleaner.cleaning do
			example.run
		end
	end
end
