require 'rails_helper'
require 'contexts/for_models'

RSpec.shared_context 'raises Error exception' do
	it {
		expect {
			SignUpUser.with(
				user: user,
				email: email,
				password: password,
				name: name
			)
		}.to raise_exception Error
	}
end

RSpec.describe SignUpUser do
	let( :password ) { 'password' }
	let( :user ) { build :user }
	let( :name ) { user.name }
	let( :email ) { user.email }

	context 'with correct params' do
		it 'the user must be active for authentication' do
			response = SignUpUser.with(
				user: user, email: user.email, password: password, name: name
			)
			expect( response.active_for_authentication? ).to eq true
		end
	end

	context 'when email has already been taken' do
		let( :duplicate_user ) { create :user }
		let( :email ) { duplicate_user.email }
		let( :user ) { build :user, email: duplicate_user.email }
		include_context 'raises Error exception'
	end

	context 'without password' do
		let( :password ) { nil }
		include_context 'raises Error exception'
	end

	context 'without email' do
		let( :email ) { nil }
		include_context 'raises Error exception'
	end

	context 'with invalid name' do
		let( :name ) { nil }
		include_context 'raises Error exception'
	end
end
