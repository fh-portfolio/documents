require 'rails_helper'

RSpec.describe GetProjects do
  let( :user ) { create :user }
  let( :call_interactor ) { GetProjects.ownedBy owner: user }

  context 'when there are no projects' do
    it 'returns an empty array' do
      expect( call_interactor ).to match_array []
    end
  end

  context 'when there are projects' do
    let!( :a_project ) { create :project, owner: an_owner }
    let!( :another_project ) { create :project, owner: an_owner }

    context 'but not owned by the user' do
      let( :an_owner ) { create :user }

      it 'returns an empty array' do
        expect( call_interactor ).to match_array []
      end
    end

    context 'owned by the user' do
      let( :an_owner ) { user }

      it 'returns the projects' do
        expect( call_interactor ).to match_array [ a_project, another_project ]
      end
    end
  end
end