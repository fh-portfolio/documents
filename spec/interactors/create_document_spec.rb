require 'rails_helper'
require 'contexts/for_models'

RSpec.shared_examples 'CreateDocument raises an exception' do |error|
  it {
    expect{ call_interactor }.to raise_exception error
  }
end

RSpec.describe CreateDocument do
  include_context 'create user', :user
  let!( :project ) { create :project }
  let!( :file ) { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'support', 'documents', 'testapi.txt'), 'text/plain') }
  let( :call_interactor ) { CreateDocument.with file: file, project: project }

  context 'with correct params' do
    it 'creates one document' do
      expect{call_interactor}.to change{Document.count}.by 1
    end

    it 'creates the document with the correct attributes' do
      expect(call_interactor).to have_attributes({ project: project })
    end
  end

  context 'without project' do
    let( :project ) { nil }
    include_context 'CreateDocument raises an exception', ActiveRecord::RecordInvalid
  end

  context 'without file' do
    let( :file ) { nil }
    include_context 'CreateDocument raises an exception', ActiveRecord::RecordInvalid
  end

  context 'with a non suported file extension' do
    let(:file) { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'support', 'images', 'rails.png'), 'image/png') }
    include_context 'CreateDocument raises an exception', ActiveRecord::RecordInvalid
  end
end
