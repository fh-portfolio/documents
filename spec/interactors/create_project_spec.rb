require 'rails_helper'
require 'contexts/for_models'

RSpec.shared_examples 'CreateProject raise error' do |error|
  it {
    expect { call_interactor }.to raise_exception error
  }
end


RSpec.describe CreateProject do
  include_context 'create user', :user

  let( :owner ) { user }
  let( :name ) { 'a Project' }
  let( :description ) { 'A project description' }
  let( :params ) {
    {
      owner: owner,
      name: name,
      description: description
    }
  }
  let( :call_interactor ) {
    CreateProject.with params
  }

  context 'with correct params' do
    it 'create a project' do
      expect{ call_interactor }.to change{ Project.count }.by( 1 )
    end

    it 'create a project with the correct attributes' do
      created_project = call_interactor
      expect( created_project ).to have_attributes( params )
    end
  end

  context 'without description' do
    let( :description ) { nil }
    include_context 'CreateProject raise error', ActiveRecord::RecordInvalid
  end

  context 'without owner' do
    let( :owner ) { nil }
    include_context 'CreateProject raise error',  ActiveRecord::RecordInvalid
  end

  context 'without name' do
    let( :name ) { nil }
    include_context 'CreateProject raise error', ActiveRecord::RecordInvalid
  end

  context 'with a name that already exists' do
    let!( :project ) { create :project }
    let( :name ) { project.name }

    include_context 'CreateProject raise error', ActiveRecord::RecordInvalid
  end
end
