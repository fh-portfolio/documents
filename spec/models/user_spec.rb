require 'rails_helper'

RSpec.describe User, type: :model do
	it { is_expected.to respond_to :name }
	it { is_expected.to respond_to :password }
	it { is_expected.to respond_to :email }
	it { is_expected.to respond_to :avatar }

	describe 'User' do
		let( :user ) {  create :user }

		context 'with correct email and password' do
			it 'is valid' do
				expect(
					User.new email: 'valid@app.com', password: 'password'
				).to be_valid
			end
		end

		context 'with an invalid email format' do
			it 'the is not invalid' do
				expect(
					User.new email: 'invalid-app.com', password: 'password'
				).not_to be_valid
			end
		end

		context 'without a password' do
			it 'is not invalid' do
				expect(
					User.new email: 'valid@app.com', password: nil
				).to_not be_valid
			end
		end
	end
end
