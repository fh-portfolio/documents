require 'rails_helper'

RSpec.describe Project, type: :model do
  # A user needs to be created before to allow #Matcher to validate fields uniqueness
  let!(:a_project) { create :project }

  it { is_expected.to respond_to :name }
  it { is_expected.to respond_to :description }
  it { is_expected.to respond_to :owner }
  it { is_expected.to respond_to :created_at }
  it { is_expected.to respond_to :updated_at }
  it { is_expected.to respond_to :owner_id }
  it { is_expected.to belong_to :owner }


  it { is_expected.to validate_presence_of :name }
  it { is_expected.to validate_uniqueness_of :name }
  it { is_expected.to validate_presence_of :description }
  it { is_expected.to validate_presence_of :owner }
end
