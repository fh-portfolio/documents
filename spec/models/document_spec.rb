require 'rails_helper'

RSpec.describe Document, type: :model do
  it { is_expected.to respond_to :file }
  it { is_expected.to respond_to :project }

  it { is_expected.to validate_presence_of :file }
  it { is_expected.to validate_presence_of :project }

  let(:document) { build :document }
  let(:txt_document) { build :document, :with_a_txt_file }
  let(:doc_document) { build :document, :with_a_doc_file }
  let(:jpg_document) { build :document, :with_a_jpg_file }
  context 'with a valid extension' do
    it 'is valid' do
      expect(txt_document).to be_valid
      expect(doc_document).to be_valid
    end
  end

  context 'with an invalid extension' do
    it 'is invalid' do
      expect(jpg_document).to be_invalid
    end
  end

  context 'with a repeated basename' do
    it 'is invalid' do
      txt_document.save!
      expect(doc_document).to be_invalid
    end

    it 'has the correct error' do
      txt_document.save!
      doc_document.valid?
      expect(doc_document.errors[:file]).to include 'filenames must be unique'
    end
  end
end

