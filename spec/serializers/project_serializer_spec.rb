require 'rails_helper'
require 'contexts/for_models'

RSpec.describe ProjectSerializer do
  let(:project) { create :project }
  let(:project_serialization) {
    ActiveModelSerializers::SerializableResource.new(project, {}).as_json
  }

  it 'renders the project correctly' do
    expect(project_serialization).to eq (
      {
        id: project.id,
        name: project.name,
        description: project.description,
        created_at: project.created_at,
        updated_at: project.updated_at,
        documents: project.documents
      }
    )
  end
end
