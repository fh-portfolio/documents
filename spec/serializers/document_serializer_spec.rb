require 'rails_helper'
require 'shared_examples/for_serializers'

RSpec.describe DocumentSerializer do
  let(:document) { create :document }

  let(:file_url) { Settings.urls.base + document.file.url }
  let(:serializable_object) { document }
  let(:expected_serialization) {
    {
        id: document.id,
        file_url: file_url,
        description: document.description
    }
  }

  include_examples "expect correct serialization"
end
