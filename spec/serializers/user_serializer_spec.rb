require 'rails_helper'
require 'contexts/for_models'

RSpec.describe UserSerializer do
	include_context 'create user', :user
	let( :user_serialization ) { 
		ActiveModelSerializers::SerializableResource.new( user, {} ).as_json 
	}

	it 'renders the user correctly' do
		expect( user_serialization ).to eq(
			{
				id: user.id,
				email: user.email,
				name: user.name,
				avatar: user.avatar
			}
		)
	end
end
