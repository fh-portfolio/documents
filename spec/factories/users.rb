FactoryBot.define do
	factory :user do
		sequence( :name ) { |n| "name-#{n}" }
		sequence( :password ) { |n| "password-#{n}" }
		sequence( :email ) { |n| "example#{n}@example.com" }
		avatar Rack::Test::UploadedFile.new(
			File.join(Rails.root, 'spec', 'support', 'images', 'rails.png'), 'image/png'
		)
	end
end
