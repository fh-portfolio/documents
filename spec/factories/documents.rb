FactoryBot.define do
  factory :document do
    file Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'support', 'documents', 'testapi.txt'), 'text/plain')
    description 'A document'
    project
  end

  trait :with_a_doc_file do
    file Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'support', 'documents', 'testapi.doc'), 'application/msword')
  end

  trait :with_a_txt_file do
    file Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'support', 'documents', 'testapi.txt'), 'text/plain')
  end

  trait :with_a_jpg_file do
    file Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'support', 'images', 'rails.jpg'), 'image/jpg')
  end
end
