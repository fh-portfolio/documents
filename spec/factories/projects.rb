FactoryBot.define do
  factory :project do
    sequence :name do |n| "Project #{n}" end
    description "Description"
    association :owner, factory: :user
  end
end
