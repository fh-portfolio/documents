require 'rails_helper'
require 'shared_examples/for_controllers'
require 'contexts/for_models'
require 'contexts/for_controllers'


RSpec.shared_examples 'expect invalid error with error message' do
  let(:error) { 'invalid_record' }

  include_context 'post request'
  include_examples 'expect unprocessable entity response'
  include_examples 'expect correct error response'
end

RSpec.describe Api::V1::DocumentsController, type: :controller do
  let(:a_document) { create :document }
  let(:project) { create :project }
  let(:description) { 'A description' }
  let(:get_action) { :index_for_project }

  describe '#create' do
    let( :file ) { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'support', 'documents', 'testapi.txt'), 'text/plain') }

    let(:post_action) { :create }
    let(:params) { { project_id: project.id, file: file, description: description } }

    context 'without token' do
      include_context 'post request'
      include_examples 'expect unauthorized response'
    end

    context 'with token' do
      include_context 'stub doorkeeper'

      context 'with valid params' do

        include_context 'post request'
        include_examples 'expect successful response'
      end

      context 'with invalid params' do
        context 'without a file' do
          let(:file) { nil }
          let(:error_message) { { file: ['can\'t be blank'] } }

          include_examples 'expect invalid error with error message'
        end

        context 'with a name that already exists' do
          let!(:another_document) { create :document, project: project, file: file }
          let!(:project) { create :project }
          let(:name) { project.name }
          let(:error_message) { { file: ['filenames must be unique'] } }

          include_examples 'expect invalid error with error message'
        end
      end
    end
  end
end
