require 'rails_helper'
require 'shared_examples/for_controllers'
require 'contexts/for_controllers'

RSpec.shared_examples 'expect invalid error with error message' do
  let(:error) { 'invalid_record' }

  include_context 'post request'
  include_examples 'expect unprocessable entity response'
  include_examples 'expect correct error response'
end

RSpec.shared_examples 'requires authentication' do | request_method |
  context 'without token' do
      include_context request_method
      include_examples 'expect unauthorized response'
    end
end

RSpec.describe Api::V1::ProjectsController, type: :controller do
  describe '#create' do
    let( :post_action ) { :create }
    let( :name ) { :create }
    let( :name ) { 'A name' }
    let( :description ) { 'A description' }
    let( :params ) {
      {
          name: name,
          description: description
      }
    }

    include_examples 'requires authentication', 'post request'

    context 'with token' do
      let( :response_collection ) { ProjectSerializer.new Project.last, scope: { current_user: user } }

      include_context 'stub doorkeeper'

      context 'with correct params' do
        include_context 'post request'
        include_examples 'expect successful response'
        include_examples 'expect correct collection response'
      end

      context 'with invalid params' do
        context 'without a name' do
          let(:name) { nil }
          let(:error_message) { { name: ['can\'t be blank'] } }

          include_examples 'expect invalid error with error message'
        end

        context 'without a description' do
          let(:description) { '' }
          let(:error_message) { { description: ['can\'t be blank'] } }

          include_examples 'expect invalid error with error message'
        end

        context 'with a name that already exists' do
          let!(:project) { create :project }
          let(:name) { project.name }
          let(:error_message) { { name: ['has already been taken'] } }

          include_examples 'expect invalid error with error message'
        end
      end
    end
  end

  describe '#index' do
    let(:get_action) { :index }
    let(:params) { {} }

    include_examples 'requires authentication', 'get request'

    context 'with token' do
      include_context 'stub doorkeeper'

      context 'without projects' do
        let( :response_collection ) { [] }

        include_context 'get request'
        include_examples 'expect successful response'
        include_examples 'expect correct collection response'
      end

      context 'with two projects' do
        let!( :a_project ) { create :project, owner: owner }
        let!( :another_project ) { create :project, owner: owner }
        let!(:projects) { [ a_project, another_project ]}

        context  'owned by another user' do
          let( :owner ) { create :user }
          let( :response_collection ) { [] }

          include_context 'get request'
          include_examples 'expect successful response'
          include_examples 'expect correct collection response'
        end

        context  'owned by the logged user' do
          let( :owner ) { user }
          let( :response_collection ) { projects }

          include_context 'get request'
          include_examples 'expect successful response'
          include_examples 'expect correct collection response'
        end
      end
    end
  end
end
