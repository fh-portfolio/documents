require 'rails_helper'
require 'shared_examples/for_controllers'
require 'contexts/for_models'
require 'contexts/for_controllers'

RSpec.shared_context 'sign up user' do
	before {
		post :create, params: {
			user: {
				email: email, password: password, name: name
			}
		}
	}
end

RSpec.describe Api::V1::RegistrationsController, type: :controller do
	before(:each) do
		@request.env["devise.mapping"] = Devise.mappings[:user]
	end

	describe '#create' do
		let( :email ) { 'me@example.com' }
		let( :password ) { 'password' }
		let( :name ) { "Cosme Fulanito" }

		context 'with correct params' do
			include_context 'sign up user'
			include_examples 'expect successful response'
		end

		context 'with invalid email' do
			let( :email ) { nil }

			include_context 'sign up user'
			include_examples 'expect bad request response'
		end

		context 'with invalid password' do
			let( :password ) { nil }

			include_context 'sign up user'
			include_examples 'expect bad request response'
		end

		context 'with invalid name' do
			let( :name ) { nil }

			include_context 'sign up user'
			include_examples 'expect bad request response'
		end
	end
end
