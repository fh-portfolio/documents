require 'rails_helper'
require 'shared_examples/for_controllers'
require 'contexts/for_models'
require 'contexts/for_controllers'

RSpec.shared_context 'get user' do
	before { get :show, params: { id: "me" } }
end

RSpec.shared_context 'forgot_password' do
	before { post :forgot_password, params: { email: email } }
end

RSpec.describe Api::V1::UsersController, type: :controller do
	include_context 'create user', :user

	describe 'Get user' do
		context 'without token' do
			include_context 'get user'
			include_examples 'expect unauthorized response'
		end

		context 'with token' do
			include_context 'stub doorkeeper'

			include_context 'get user'
			include_examples 'expect successful response'
		end
	end

	describe '#forgot_password' do
		before(:each) do
			@request.env["devise.mapping"] = Devise.mappings[:user]
		end

		let( :user ) { create :user }
		let( :email ) { user.email }

		context 'with correct params' do
			include_context 'forgot_password'
			include_examples 'expect successful response'
		end

		context 'with invalid email' do
			let( :email ) { 'no@idea.com' }

			include_context 'forgot_password'
			include_examples 'expect not found response'
		end
	end
end
