require 'rails_helper'
require 'shared_examples/for_controllers'
require 'contexts/for_controllers'

RSpec.shared_context 'get terms and conditions page' do
	before { get :show, params: { } }
end

RSpec.describe TermsAndConditionsController, type: :controller do
	context "#show" do
		include_context 'get terms and conditions page'

		it { expect( response.content_type ).to eq( "text/html" ) }
		it { expect( response.body ).to eq "" }
		it { expect( response.status ).to eq 200 }
	end
end