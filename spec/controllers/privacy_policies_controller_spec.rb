require 'rails_helper'
require 'shared_examples/for_controllers'
require 'contexts/for_controllers'

RSpec.shared_context 'get privacy policies page' do
	before { get :show, params: { } }
end

RSpec.describe PrivacyPoliciesController, type: :controller do
	context "#show" do
		include_context 'get privacy policies page'

		it { expect( response.content_type ).to eq( "text/html" ) }
		it { expect( response.body ).to eq "" }
		it { expect( response.status ).to eq 200 }
	end
end