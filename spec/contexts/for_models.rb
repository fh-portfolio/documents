RSpec.shared_context 'create user' do |role|
	let( :user ) { create :user }
	before { user.add_role role }
end

RSpec.shared_context 'create user with device' do
	include_context 'create user', :user
	let( :device ) { create :device, user: user }
end
