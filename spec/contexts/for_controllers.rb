require 'contexts/for_models'

RSpec.shared_context 'stub doorkeeper' do
	let( :token ) { double :acceptable? => true }
	include_examples 'create user', :user

	before do
		allow( controller ).to receive( :doorkeeper_token ) { token }
		allow( controller ).to receive( :current_user ) { user }
	end
end

RSpec.shared_context 'get request' do
	before { get get_action, { params: params } }
end

RSpec.shared_context 'post request' do
	before { post post_action, { params: params } }
end

RSpec.shared_context 'patch request' do
	before { patch patch_action, { params: params } }
end

RSpec.shared_context 'delete request' do
	before { delete delete_action, { params: params } }
end
