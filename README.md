# Documents

This is a sample and simple API to store and retrieve simple projects with every version that was uploaded.

## Setup

- Clone the project into a new directory 

```sh
$ git clone git@gitlab.com:fh-portfolio/documents.git
```

## Run the server with Docker

The project is Docker-ready. To start developing we just need to have Docker installed.

```sh
cp config/database.yml.example config/database.yml
cp config/secrets.yml.example config/secrets.yml
cp config/settings/example.yml config/settings/test.yml
cp config/settings/example.yml config/settings/development.yml
cp .env.dev .env
cp Dockerfile.dev Dockerfile
docker-compose build && docker-compose run app bundle
docker-compose run app rake db:create db:migrate db:seed
docker-compose up -d && docker attach app_documents
```

## CI/CD

The CI/CD is set up and the test run every time that there is a push to the repository.
It can be seen in the [Pipelines URL](https://gitlab.com/fh-portfolio/documents/pipelines)

## Reports

The rake task *app:reports* is used to generate all of the following reports.

### Tests

The test can be run with

```sh
$ docker-compose run app rspec
```

### Code Review

Automatic code review is implemented using [Rubycritic][6] and [Rubocop][7]. Rubycritic is used for code smells, and Rubocop
for style conventions and other syntax issues.

- Rubycritic is run with *app:codereview*.
- Rubocop is run using *app:lint*.

```sh
docker-compose run app bundle exec rake app:codereview
```

- Rubycritic output is saved at `reports/codereview/overview.html`.
- Rubocop output is saved at `reports/lint.html`.

The rake task *app:lint:auto_corret* tries to auto correct errors detected by Rubocop.

### Code Coverage

[SimpleCov][4] is used for code coverage reports. To generate a coverage report from tests use the rake task *app:coverage*.

```sh
docker-compose run app bundle exec rake app:coverage
```

The report is saved at `reports/coverage/index.html`.

## Rake Tasks

- *app:lint*: Code style and syntax issues report generator (report task).
- *app:lint:auto_correct*: Auto-correct Lint issues.
- *app:codereview*: Automatic code-review report (report task).
- *app:test* or *spec*: Run all tests in spec directory.
- *app:coverage*: Generate code coverage report from tests (report task).
- *app:reports*: Run all the report generation tasks (report task).

## API

### Documentation
The documentation is done with Postman. It can be run through the application or via web.
The localhost server must be running for this collection to work.

[Run in web](https://documenter.getpostman.com/view/6330176/SVn3raPi?version=latest) 

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/306115b35754c56bac37#?env%5Bdocuments%20localhost%5D=W3siZGVzY3JpcHRpb24iOnsiY29udGVudCI6IiIsInR5cGUiOiJ0ZXh0L3BsYWluIn0sInZhbHVlIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwia2V5Ijoic2VydmVyX3VybCIsImVuYWJsZWQiOnRydWV9LHsidmFsdWUiOiI2N2ZkNmFiMDc1NzhhZDZlOWQyM2U2ZWZhMzJiODRjNjgzZTkwMzExMDE2NGUwYWVkYjYzMjE5YjE5YThjOTRkIiwia2V5IjoiYWNjZXNzX3Rva2VuIiwiZW5hYmxlZCI6dHJ1ZX1d)

### References
[1]: http://rspec.info/
[2]: https://github.com/rspec/rspec-rails
[3]: http://agiledata.org/essays/tdd.html
[4]: https://github.com/colszowka/simplecov
[5]: http://yardoc.org/
[6]: https://github.com/whitesmith/rubycritic
[7]: https://github.com/bbatsov/rubocop
[8]: http://raml.org/
[9]: /docs/api/main.raml
[10]: https://github.com/mulesoft/api-console
[11]: https://github.com/kevinrenskers/raml2html
